# UI MAP / OBJECT REPOSITORIES
***
## Pengertian
Ui Map atau Object Repository adalah sebuah konsep untuk mendefine, menyimpan, dan menyajikan elemen-elemen UI dari
suatu aplikasi atau website. Pada konsepnya UI Map atau Object Repository akan disimpan didalam 1 atau lebih file agar
lebih teroganisir dan mudah untuk di maintain kedepannya.
***
## Manfaat / Fungsi
1. Pengelolaan yang Terorganisir: UI Map membantu dalam pengorganisasian dan dokumentasi elemen-elemen UI yang akan
   digunakan dalam skenario pengujian, memudahkan pemeliharaan dan manajemen kode pengujian.

2. Keterbacaan dan Keterpahaman yang Tinggi: Dengan UI Map, pengembang dan pengujian dapat dengan mudah melihat dan
   memahami struktur antarmuka pengguna aplikasi, serta mengidentifikasi elemen-elemen yang diperlukan untuk menguji fungsionalitas tertentu.

3. Fleksibilitas dan Reusabilitas: UI Map memungkinkan pengujian untuk disusun secara modular dan dapat digunakan kembali
   dalam skenario pengujian yang berbeda, mempercepat proses pengembangan dan pengujian perangkat lunak.