package stockbit;

import io.github.cdimascio.dotenv.Dotenv;

import static stockbit.utils.Constants.ELEMENTS;
import static stockbit.utils.Constants.SRC_TEST_RESOURCES;
import static stockbit.utils.Utils.loadElementProperties;

public class Debug {
    public static void main(String[] args) {
        Dotenv dotenv = Dotenv.load();
        System.out.println(dotenv.get("USERNAMES"));
    }
}
